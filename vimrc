" Leader
let mapleader=","

" Vim-polyglot
let g:polyglot_disabled = ['sensible']
let g:polyglot_disabled = ['markdown']
let g:polyglot_disabled = ['markdown.plugin']
let g:polyglot_disabled = ['autoindent']
autocmd BufEnter * set indentexpr=

" Plug
call plug#begin('~/.vim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'plasticboy/vim-markdown'
Plug 'mbbill/undotree'
Plug 'godlygeek/tabular'
Plug 'lilydjwg/colorizer'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'preservim/nerdtree'
Plug 'vimwiki/vimwiki'
Plug 'sheerun/vim-polyglot'
Plug 'junegunn/goyo.vim'
call plug#end()

" Appearance
"let g:gruvbox_termcolors=256
set termguicolors
syntax on
let g:lightline = {
      \ 'colorscheme': 'materia',
      \ }
if !has('gui_running')
set t_Co=256
set t_ut=
endif
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
set noshowmode
"colorscheme gruvbox
set textwidth=80
set wrapmargin=2
set showmatch

" Tab Settings "
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4

" Cursor Settings
"set cursorline
"set cursorcolumn
set scrolloff=5

" Security
set modelines=0

" For plugins to load correctly
filetype plugin indent on

" Show file status
set ruler

" Avoid the ESC Mode
inoremap ii <ESC>
inoremap <ESC> <NOP>
vnoremap ii <ESC>
vnoremap <ESC> <NOP>
cnoremap ii <ESC>
cnoremap <ESC> <NOP>

" Enable spell checking, s for spell check
map <leader>s :setlocal spell! spelllang=en_us

" Autocompletion
set wildmenu
set wildmode=longest,list,full

" Basics
filetype off
set autoindent
set smartindent
set hlsearch
set nowrap
set clipboard+=unnamedplus
set shiftwidth=1
set noerrorbells
set ignorecase
set smartcase
set history=5000
set background=dark
set noswapfile
set incsearch
set expandtab
set fileformat=unix
filetype plugin on
set number relativenumber
set undofile
set undodir=~/.vim/undodir
set laststatus=2

" Disable the arrow keys
map <Up> <Nop>
map <Left> <Nop>
map <Right> <Nop>
map <Down> <Nop>

" Vimwiki
let g:vimwiki_list = [{'path': '~/.local/share/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
set splitbelow splitright

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"Newtab with ctrl+t
nnoremap <silent> <C-t> :tabnew<CR>

"Paste from system clipboard with ctrl+i instead of shift insert
"map <S-Insert> <C-i>

" Automatically deletes all trailing whitespace on save.
autocmd BufWritePre * %s/\s\+$//e

" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufWritePost *xresources,*xdefaults !xrdb %

" Nerd tree
map <leader>t :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
  if has('vim')
    let NERDTreeBookmarksFile = stdpath('data') . '/NERDTreeBookmarks'
  else
    let NERDTreeBookmarksFile = '~/.vim' . '/NERDTreeBookmarks'
  endif
"autocmd FileType nerdtree :setlocal noreadonly

" Centering Document when entering insert mode
"autocmd InsertEnter * norm zz

" Alis replace all to S
noremap S :%s/gI<Left><Left><Left>

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" fzf
let g:fzf_preview_window = 'right:50%'
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6  }  }
