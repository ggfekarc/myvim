" Leader
let mapleader=","

" Plugins
call plug#begin('~/.local/share/nvim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'ap/vim-css-color'
call plug#end()

" Apprearance
let g:lightline = {
      \ 'colorscheme': 'one',
      \ }
set termguicolors

" Tab Settings "
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4

" Cursor Settings
"set cursorline
"set cursorcolumn
set scrolloff=5

" Security
set modelines=0

" For plugins to load correctly
filetype plugin indent on

" Show file status
set ruler

" Avoid the ESC Mode
inoremap ii <ESC>
inoremap <ESC> <NOP>
vnoremap ii <ESC>
vnoremap <ESC> <NOP>
cnoremap ii <ESC>
cnoremap <ESC> <NOP>

" Enable spell checking, s for spell check
map <leader>s :setlocal spell! spelllang=en_us

" Autocompletion
set wildmenu
set wildmode=longest,list,full

" Basics
filetype off
set noshowmode
set autoindent
set smartindent
set hlsearch
set nowrap
set clipboard+=unnamedplus
set shiftwidth=1
set noerrorbells
set ignorecase
set smartcase
set history=5000
set background=dark
set noswapfile
set incsearch
set expandtab
set fileformat=unix
filetype plugin on
set number relativenumber
set undofile
set undodir=~/.vim/undodir
set laststatus=2

" Disable the arrow keys
map <Up> <Nop>
map <Left> <Nop>
map <Right> <Nop>
map <Down> <Nop>

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"Newtab with ctrl+t
nnoremap <silent> <C-t> :tabnew<CR>

" Automatically deletes all trailing whitespace on save.
autocmd BufWritePre * %s/\s\+$//e

" Alis replace all to S
noremap S :%s/<Left><Left><Left>
